import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.FrameWindow;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HrefCollector {
    private final WebClient webClient;

    public HrefCollector(WebClient webClient) {
        this.webClient = webClient;
        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
    }

    public Set<String> getAllHrefs(String url) {

        Set<String> actualUrls = getHrefs(url);
        int size = -1;
        Set<String> allUrls = actualUrls;
        if (actualUrls.isEmpty()) {
            System.out.println("Ezen az oldalon nincs más hivatkozás: " + url);
        } else {
            while (size < allUrls.size()) {
                size = allUrls.size();
                System.out.println("az összes eddigi hivatkozás száma: " + size);
                Set<String> newUrls = doRecursion(actualUrls);
                allUrls = mergeSets(allUrls, newUrls);
                actualUrls = newUrls;
            }
        }
        return allUrls;
    }

    public int getShortestPath(String url1, String url2) {

        Set<String> actualUrls = getHrefs(url1);
        Set<String> allUrls = actualUrls;
        int size = -1;
        int lengthOfPath = 0;
        if (actualUrls.isEmpty()) {
            System.out.println("az első url által megadott oldalon nincs másik hivatkozás");
        } else {
            while (!actualUrls.contains(url2) || size < allUrls.size()) {
                lengthOfPath++;
                size = allUrls.size();
                Set<String> newUrls = doRecursion(actualUrls);
                allUrls = mergeSets(allUrls, newUrls);
                actualUrls = newUrls;
            }
        }
        return lengthOfPath;
    }

    private Set<String> doRecursion(Set<String> actualUrls) {

        return actualUrls.stream()
                .map(this::getHrefs)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    private Set<String> getHrefs(String url) {

        HtmlPage startPage;
        try {
            startPage = (HtmlPage) webClient.getPage(url);
            List<HtmlElement> items = startPage.getByXPath("//a");
            return items.stream().map(he -> he.getAttributeDirect("href")).collect(Collectors.toSet());
        } catch (IOException e) {
            return Collections.emptySet();
        }
    }

    public static <T> Set<T> mergeSets(Set<T> a, Set<T> b) {
        return Stream.of(a, b)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }


}