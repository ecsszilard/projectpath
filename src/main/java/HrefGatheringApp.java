import com.gargoylesoftware.htmlunit.WebClient;

import java.util.Set;

public class HrefGatheringApp {

    public static void main(String[] args) {

        final HrefCollector hrefCollector = new HrefCollector(new WebClient());

        Set<String> hrefs = hrefCollector.getAllHrefs("http://localhost:8080/bills/");
        for (String href : hrefs) {
            System.out.println(href);
        }

        String testHref1 = "http://localhost:8080/bills/";
        String testHref2 = "/bills/2";

        if (hrefs.contains(testHref2)) {
            int lengthOfPath = hrefCollector.getShortestPath(testHref1, testHref2);
            System.out.println("a két url közti távolság: " + lengthOfPath);
        } else {
            System.out.println("nincs ilyen url");
        }


    }
}
